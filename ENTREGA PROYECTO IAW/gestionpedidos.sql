-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-02-2020 a las 10:20:38
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestionpedidos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblclientes`
--

CREATE TABLE `tblclientes` (
  `idCliente` int(11) NOT NULL,
  `strDni` varchar(20) NOT NULL,
  `strNombre` varchar(50) DEFAULT NULL,
  `strApellidos` varchar(100) DEFAULT NULL,
  `strDireccion` varchar(100) DEFAULT NULL,
  `strLocalidad` varchar(50) DEFAULT NULL,
  `intProvincia` int(50) DEFAULT NULL,
  `strCp` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblclientes`
--

INSERT INTO `tblclientes` (`idCliente`, `strDni`, `strNombre`, `strApellidos`, `strDireccion`, `strLocalidad`, `intProvincia`, `strCp`) VALUES
(6, '48330190Z', 'Fco', 'Marcet', ' C/ Mediterraneo 10', 'villajoyosa', 1, '03570'),
(7, '14589632G', 'Luis', 'Gutierrez Marcet', ' C/ polop 55', 'villajoyosa', 2, '02547'),
(8, '78963258K', 'Lucia', 'Martinez', ' C/ polop 666', 'Elda', 1, '02589'),
(10, '12345678Z', 'Antonio', 'aaaaaa', ' C/ Ferrocarril', 'La Vila', 1, '03570'),
(11, '14456789D', 'Pepe', 'Lotas', ' C/ Ferrocarril 33', 'Alicante', 2, '02558'),
(20, '1452365G', 'Lucia', 'Garcia', ' Callle 123345', 'La Vila Joiosa', 3, '02578'),
(23, '14789563K', 'Lucia', 'Lapiedra', ' C/ polop 256', 'Villajoyosa', 2, '02589');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblpedidos`
--

CREATE TABLE `tblpedidos` (
  `idPedido` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `dtmFecha` date NOT NULL,
  `strPeso` varchar(15) DEFAULT NULL,
  `strAlto` varchar(15) DEFAULT NULL,
  `strAncho` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblpedidos`
--

INSERT INTO `tblpedidos` (`idPedido`, `idcliente`, `dtmFecha`, `strPeso`, `strAlto`, `strAncho`) VALUES
(6, 8, '2020-11-05', '147852', '100', '100'),
(7, 6, '2020-12-12', '100', '100', '100'),
(8, 7, '2020-01-01', '100', '100', '100'),
(10, 6, '2020-02-21', '258', '258', '258'),
(11, 8, '2020-11-11', '1234', '1234', '1234'),
(12, 6, '2020-12-12', '111', '123', '77');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblprovincias`
--

CREATE TABLE `tblprovincias` (
  `idProvincia` int(11) NOT NULL,
  `strNombreProvincia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tblprovincias`
--

INSERT INTO `tblprovincias` (`idProvincia`, `strNombreProvincia`) VALUES
(1, 'Alicante'),
(2, 'Valencia'),
(3, 'Castellon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuarios`
--

CREATE TABLE `tblusuarios` (
  `idUsuario` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `strPasswd` varchar(264) NOT NULL,
  `strNivel` varchar(50) NOT NULL,
  `dtmFechaReg` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblusuarios`
--

INSERT INTO `tblusuarios` (`idUsuario`, `strNombre`, `strPasswd`, `strNivel`, `dtmFechaReg`) VALUES
(4, 'admin', '$2y$10$Z27TwtLmxfmLPEKIowAquuMjp9CB2J/qfoWMSHW0CICqPyb5vonRi', 'administrador', '2020-01-28 20:23:08'),
(6, 'invitado', '$2y$10$C/ZmLqbcZMTtaHsv0u53NOCs4zoQF8/H9bvJ0vlXgAbfzajAkvZAO', 'invitado', '2020-02-06 11:49:52'),
(7, 'antonio', '$2y$10$71e/4jsqc1F5CoTCLm8akuP4TdGlG1Up7RzLGxYo0eL95ajQVZGm.', '', '2020-02-10 10:00:20'),
(8, 'pepelotas', '$2y$10$iDyV1rba9emNlsYm3F1kI..kLGj7hJLrzVr2mYeRz/9ycaZCu8mLK', '', '2020-02-13 09:25:35'),
(9, 'bonifacio', '$2y$10$iPM4uPltYscwo1JHsHhO6eLgt8NWDCKWrTsZO9pdviHeKSBS17N5m', '', '2020-02-13 09:26:16');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblclientes`
--
ALTER TABLE `tblclientes`
  ADD PRIMARY KEY (`idCliente`),
  ADD KEY `fk_intPedido_idx` (`intProvincia`);

--
-- Indices de la tabla `tblpedidos`
--
ALTER TABLE `tblpedidos`
  ADD PRIMARY KEY (`idPedido`),
  ADD KEY `fk_idcliente_idx` (`idcliente`);

--
-- Indices de la tabla `tblprovincias`
--
ALTER TABLE `tblprovincias`
  ADD PRIMARY KEY (`idProvincia`);

--
-- Indices de la tabla `tblusuarios`
--
ALTER TABLE `tblusuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblclientes`
--
ALTER TABLE `tblclientes`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `tblpedidos`
--
ALTER TABLE `tblpedidos`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tblprovincias`
--
ALTER TABLE `tblprovincias`
  MODIFY `idProvincia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tblusuarios`
--
ALTER TABLE `tblusuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tblclientes`
--
ALTER TABLE `tblclientes`
  ADD CONSTRAINT `tblclientes_ibfk_1` FOREIGN KEY (`intProvincia`) REFERENCES `tblprovincias` (`idProvincia`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `tblpedidos`
--
ALTER TABLE `tblpedidos`
  ADD CONSTRAINT `tblpedidos_ibfk_1` FOREIGN KEY (`idcliente`) REFERENCES `tblclientes` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
