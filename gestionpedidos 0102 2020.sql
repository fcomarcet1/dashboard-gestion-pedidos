-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-02-2020 a las 12:39:05
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestionpedidos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblclientes`
--

CREATE TABLE `tblclientes` (
  `idCliente` int(11) NOT NULL,
  `strDni` varchar(20) NOT NULL,
  `strNombre` varchar(50) DEFAULT NULL,
  `strApellidos` varchar(100) DEFAULT NULL,
  `strDireccion` varchar(100) DEFAULT NULL,
  `strLocalidad` varchar(50) DEFAULT NULL,
  `strProvincia` varchar(50) DEFAULT NULL,
  `strCp` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblclientes`
--

INSERT INTO `tblclientes` (`idCliente`, `strDni`, `strNombre`, `strApellidos`, `strDireccion`, `strLocalidad`, `strProvincia`, `strCp`) VALUES
(1, '48330190Z', 'Fco', 'Marcet Prieto', 'Avda Pais Valencià 35', 'Villajoyosa', 'Alicante', '3570'),
(3, '3456789F', 'Jose ', 'Perez', ' C/ Polop 234', 'Elche', 'Alicante', '09875'),
(4, '1234567K', 'pepe', 'Lotas', ' C/ Polop 234', 'Elda', 'Alicante', '098654');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblpedidos`
--

CREATE TABLE `tblpedidos` (
  `idPedido` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `dtmFecha` date DEFAULT NULL,
  `strPeso` varchar(15) DEFAULT NULL,
  `strAlto` varchar(15) DEFAULT NULL,
  `strAncho` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblpedidos`
--

INSERT INTO `tblpedidos` (`idPedido`, `idcliente`, `dtmFecha`, `strPeso`, `strAlto`, `strAncho`) VALUES
(1, 1, '2020-02-01', '50', '21', '100');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuarios`
--

CREATE TABLE `tblusuarios` (
  `idUsuario` int(11) NOT NULL,
  `strNombre` varchar(50) NOT NULL,
  `strPasswd` varchar(264) NOT NULL,
  `srtNivel` varchar(50) NOT NULL,
  `dtmFechaReg` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tblusuarios`
--

INSERT INTO `tblusuarios` (`idUsuario`, `strNombre`, `strPasswd`, `srtNivel`, `dtmFechaReg`) VALUES
(3, 'admin88', '$2y$10$FGAnN3VQirGU68gtvtlg/uG5lDVdp4lIsbDxHYmUOK5/yNPIvIape', '', '2020-01-28 17:36:21'),
(4, 'admin', '$2y$10$Z27TwtLmxfmLPEKIowAquuMjp9CB2J/qfoWMSHW0CICqPyb5vonRi', 'administrador', '2020-01-28 20:23:08');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblclientes`
--
ALTER TABLE `tblclientes`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `tblpedidos`
--
ALTER TABLE `tblpedidos`
  ADD PRIMARY KEY (`idPedido`),
  ADD KEY `idcliente` (`idcliente`);

--
-- Indices de la tabla `tblusuarios`
--
ALTER TABLE `tblusuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblclientes`
--
ALTER TABLE `tblclientes`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tblpedidos`
--
ALTER TABLE `tblpedidos`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tblusuarios`
--
ALTER TABLE `tblusuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tblpedidos`
--
ALTER TABLE `tblpedidos`
  ADD CONSTRAINT `tblpedidos_ibfk_1` FOREIGN KEY (`idcliente`) REFERENCES `tblclientes` (`idCliente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
