<?php include_once("conexiones/conexionpedidos.php"); ?>

<?php  
//acceso.php dbname=test
 
 session_start();  
 if(isset($_SESSION["usuario"])){  

      header("location:index.php");  
 }  

 if(isset($_POST["registro"]))  
 {  
      if(empty($_POST["usuario"]) || empty($_POST["passwd"]))  
      {  
           echo "<script>alert('Rellene los campos')</script>";  
      }  
      else  
      {  
           $usuario = mysqli_real_escape_string($conexionpedidos, $_POST["usuario"]);  
           $password = mysqli_real_escape_string($conexionpedidos, $_POST["passwd"]);
             
           $password = password_hash($password, PASSWORD_DEFAULT);  
           $sqlinsert = "INSERT INTO tblusuarios (strUsuario, strPasswd) 
           VALUES('$usuario', '$password')";  

           if(mysqli_query($conexionpedidos, $sqlinsert))  
           {  
                echo "<script>alert('Registro correcto')</script>";  
           }  
      }  
 }  
 if(isset($_POST["login"])){  

      if(empty($_POST["usuario"]) || empty($_POST["passwd"])){

           echo "<script>alert('Rellene los campos')</script>";  
      }  
      else{  

           $usuario = mysqli_real_escape_string($conexionpedidos, $_POST["usuario"]);  
           $password = mysqli_real_escape_string($conexionpedidos, $_POST["passwd"]);  
           $sqlselect = "SELECT * FROM tblusuarios WHERE strUsuario = '$username'";  
           $resultado = mysqli_query($conexionpedidos, $sqlselect); 

           if(mysqli_num_rows($resultado) > 0){ 

                while($row = mysqli_fetch_array($resultado))  
                {  
                     if(password_verify($password, $row["password"])){

                          $_SESSION["ususario"] = $username;  
                          header("location:index.php");  
                     }  
                     else{    
                          echo "<script>alert('Datos de acceso incorrectos')</script>";  
                     }  
                }  
           }  
           else  
           {  
                echo "<script>alert('Datos de acceso incorrectos')</script>";  
           }  
      }  
 }  
 ?>  


<!DOCTYPE html>  
 <html>  
      <head>
      <?php include_once("includes/loginmeta.php"); ?>  
        <title>Login</title>  
           
      </head>  
      <body>  
      <body class="bg-gradient-primary">

            <div class="container">

            <!-- Outer Row -->
            <div class="row justify-content-center">

                <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                        <div class="p-5">
                            <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Acceder al Sistema</h1>
                            </div>
            
                <?php  
                if(isset($_GET["action"]) == "login"){  
                    ?>  
                    <h3 align="center">Login</h3>  
                    <br />  
                    <form class="user" action="" method="POST">     
                        <div class="form-group">
                        <input type="text" name="usuario" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Introduce tu nombre de usuario..."/>
                        </div>
                        <div class="form-group">
                        <input type="text" name="passwd" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Introduce tu nombre de usuario..."/>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox small">
                                <input type="checkbox" class="custom-control-input" id="customCheck"/>
                                <label class="custom-control-label" for="customCheck">Recordar</label>
                            </div>
                        </div>
                        <input type="submit" name="login" value="Login" class="btn btn-primary btn-user btn-block" />   
                    </form>
                    div class="text-center">
                    <a class="small" href="acceso.php">Registro</a>  
                    <?php       
                }  
                else{  
                    ?>  
                    <h3 align="center">Registro</h3>  
                    <br />  
                    <form class="user" action="" method="POST">   
                        <div class="form-group">
                            <input type="text" name="usuario" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Introduce tu nombre de usuario..."/>
                        </div>
                        <div class="form-group">
                            <input type="text" name="passwd" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="Introduce tu nombre de usuario..."/>
                        </div>
                        <input type="submit" name="registro" value="Registrate" class="btn btn-info" />  
                        <br />  
                        <p align="center"><a href="acceso.php?action=login">Login</a></p>  
                    </form>  
                    <?php  
                }  
                ?>  
           </div>  
      </body>  
 </html>                 